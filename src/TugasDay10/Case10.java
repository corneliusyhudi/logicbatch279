package TugasDay10;

import java.util.*;

public class Case10 {
	
	public static void resolve1() {
		
		Scanner input = new Scanner(System.in);
		
		int i,n;
		
		
		System.out.print("Input banyak angka = ");
		n = input.nextInt();
		
		String[] angka = new String[n];
		
		System.out.println("Input angka = ");
		
		for (i=0; i<n; i++) {
			angka[i] = input.next();
		}
		

        Arrays.sort(angka, new Comparator<String>() {
            @Override
            public int compare(String angka1, String angka2) {
                if (angka1.length() < angka2.length()) {
                    return -1;
                }
                if (angka1.length() > angka2.length()) {
                    return 1;
                }
                return angka1.compareTo(angka2);
            }
        });
        
        for(String s : angka) {
        	System.out.println(s);
        }
		
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve2() {
		
		Scanner input = new Scanner(System.in);
		
		int i, j, n, temp = 0;
		
		
		System.out.print("Input banyak angka = ");
		n = input.nextInt();
		
		int[] angka = new int[n];
		
		System.out.println("Input angka = ");
		
		for (i=0; i<n; i++) {
			angka[i] = input.nextInt();
		}
		
		// 8 1 2 4 3
		
		//proses insertion sort
		for (i=0; i < n; i++) {
			temp=angka[i]; 
			j=i; 
			
			while ((j>0) && (temp<angka[j-1])) {
				angka[j]=angka[j-1];
				for(int x=0; x<n; x++)
					System.out.print(angka[x] + " ");
				System.out.println();
				j=j-1;
			}
		
			angka[j]=temp;
//			for(int x=0; x<n; x++)
//				System.out.print(angka[x] + " ");

		}
		
		for (i=0; i<n; i++) {
			System.out.print(angka[i] + " ");
		}
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve3() {
		
		Scanner input = new Scanner(System.in);
		
		int i = 0, j = 0, min = 0, max = 0, sum = 0; 
		int panjangAngka;
		int temp = 0;
		
		System.out.print("Input panjang Index angka : ");
		panjangAngka = input.nextInt();
		
		int angka[] = new int[panjangAngka];
		
		System.out.print("Input Angka = ");
		
		for (i=0; i<panjangAngka; i++) {
			angka[i] = input.nextInt();	
		}
		
		// sorting angka (ini cuman hiasan aja)
		
//		int temp = 0;
//		for (i=0;i<=panjangAngka-2;i++){
//			int minAngka=i;
//			for (j=i;j<=panjangAngka-1;j++) {
//				if (angka[j]<angka[minAngka]) {
//					minAngka=j;
//				}
//			}
//			
//		temp = angka[minAngka];
//		angka[minAngka]=angka[i];
//		angka[i]=temp;
//		}
//		
//		System.out.print("Hasil Sorting = ");
//		for(i=0; i < panjangAngka; i++) {
//			System.out.print(angka[i] + " ");
//		}
		
		for (i=0; i < panjangAngka; i++) {
			temp=angka[i]; 
			j=i; 
			
			while ((j>0) && (temp<angka[j-1])) {
				angka[j]=angka[j-1];
//				System.out.print(angka[j] + " ");
				j=j-1;
			}
		
			angka[j]=temp;
		}
		
		for (i=0; i<panjangAngka; i++) {
			System.out.print(angka[i] + " ");
		}
		
		
		
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve4() {
		
		Scanner input = new Scanner(System.in);
		
		int i, j, n, temp = 0, totalShift = 0;
		
		
		System.out.print("Input banyak angka = ");
		n = input.nextInt();
		
		int[] angka = new int[n];
		
		System.out.println("Input angka = ");
		
		for (i=0; i<n; i++) {
			angka[i] = input.nextInt();
		}
		
		// 8 1 2 4 3
		
		//proses insertion sort
		for (i=0; i < n; i++) {
			temp=angka[i]; 
			j=i; 
			
			while ((j>0) && (temp<angka[j-1])) {
				angka[j]=angka[j-1];
				j=j-1;
				totalShift++;
			}
		
			angka[j]=temp;
//			for(int x=0; x<n; x++)
//				System.out.print(angka[x] + " ");

		}
		
		for (i=0; i<n; i++) {
			System.out.print(angka[i] + " ");
		}
		System.out.println();
		System.out.print(totalShift);
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve7() {
		
		Scanner input = new Scanner(System.in);
		
		int i = 0, j = 0, min = 0, max = 0, sum = 0; 
		int panjangAngka;
		
		System.out.print("Input panjang Index angka : ");
		panjangAngka = input.nextInt();
		
		int angka[] = new int[panjangAngka];
		
		System.out.print("Input Angka = ");
		
		for (i=0; i<panjangAngka; i++) {
			angka[i] = input.nextInt();	
		}
		
		// sorting angka (ini cuman hiasan aja)
		
		int temp = 0;
		for (i=0;i<=panjangAngka-2;i++){
			int minAngka=i;
			for (j=i;j<=panjangAngka-1;j++) {
				if (angka[j]<angka[minAngka]) {
					minAngka=j;
				}
			}
			
		temp = angka[minAngka];
		angka[minAngka]=angka[i];
		angka[i]=temp;
		}
		
		System.out.print("Hasil Sorting = ");
		for(i=0; i < panjangAngka; i++) {
			System.out.print(angka[i] + " ");
		}
		
		int m = 0;
		
		if (panjangAngka % 2 == 1) {
			  m = panjangAngka / 2;
		} 
		else {
			  m = (panjangAngka - 1)/ 2;
		}
		
		System.out.println();
		System.out.print("Median = " + (double)angka[m]);
		
//		System.out.println();
//		System.out.print("Median = ");
//		for (i=0; i<panjangAngka; i++) {
//			
//			
//			System.out.println(angka[i]);
//		}
//		System.out.println(angka[i]);
//		System.out.println();
		
	}
		
	//////////////////////////////////////////////////
	
	public static void resolve8() {
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Masukkan String : ");
	    String str = input.nextLine();
	    
	    char charArray[] = str.toCharArray();
	    
	    Arrays.sort(charArray);
	    
	    System.out.println(new String(charArray));
		
	}
	
	//////////////////////////////////////////////////
	
}