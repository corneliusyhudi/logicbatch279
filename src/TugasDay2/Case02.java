package TugasDay2;

import java.util.*;

public class Case02 {
	
	public static void resolve1() {
		
		Scanner input = new Scanner(System.in);
		
		int i,ganjil=1,n;
		
		System.out.print("Banyak deret ganjil = ");
		n = input.nextInt();
		
		int arr[] = new int[n];
		
		for (i=0; i < arr.length; i++) {
			arr [i] = ganjil;
			ganjil = ganjil+2;
			System.out.print(arr[i] + " ");
		
		}
		
		
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve2() {
		
		Scanner input = new Scanner(System.in);
		
		int i,genap=0,n;
		
		System.out.print("Banyak deret genap = ");
		n = input.nextInt();
		
		int arr[] = new int[n];
		
		for (i=0; i < arr.length; i++) {
			
			genap = genap+2;
			arr [i] = genap;
			System.out.print(arr[i] + " ");
		
		}
	}
	
	/////////////////////////////////////////////////
	
	public static void resolve3() {
		Scanner input = new Scanner(System.in);
		
		int i,deret3=1,n;
		
		System.out.print("Banyak deret = ");
		n = input.nextInt();
		
		int arr[] = new int[n];
		
		for (i=0 ; i < arr.length; i++) {
			
			arr [i] = deret3;
			deret3 = deret3+3;
			System.out.print(arr[i] + " ");
			
		}
	}
	
	/////////////////////////////////////////////////
	
	public static void resolve4() {
		Scanner input = new Scanner(System.in);
		
		int i,deret4=1,n;
		
		System.out.print("Banyak deret = ");
		n = input.nextInt();
	
		int arr[] = new int[n];
		
		for (i=0 ; i < arr.length; i++) {
			
			arr [i] = deret4;
			deret4 = deret4+4;
			System.out.print(arr[i] + " ");
			
		}
	}
	
	/////////////////////////////////////////////////
	
	public static void resolve5() {
		Scanner input = new Scanner(System.in);
		
		int i,deret5=1,n;
		
		System.out.print("Banyak deret = ");
		n = input.nextInt();
		
		String arr[] = new String[n];
		
		for (i=1 ; i <=arr.length; i++) {
			if (i%3==0) {
				arr [i - 1] = "*";
			}
			else {
				arr [i - 1] = String.valueOf(deret5);
				deret5 = deret5+4;
			}
			
			System.out.print(arr[i-1] + " ");
		}
	}
	
	/////////////////////////////////////////////////
	
	public static void resolve6() {
		Scanner input = new Scanner(System.in);
		
		int i,deret6=1,n;
		
		System.out.print("Banyak deret = ");
		n = input.nextInt();
		
		String arr[] = new String[n];
		
		for (i=1 ; i <=n; i++) {
			if (i%3==0) {
				arr [i - 1] = "*";
			}
			else {
				arr [i - 1] = String.valueOf(deret6);
				
			}
			deret6 = deret6+4;
			
			System.out.print(arr[i - 1] + " ");
		
		}
	}
	
	/////////////////////////////////////////////////
	
	public static void resolve7() {
		Scanner input = new Scanner(System.in);
		
		int i,deret7=1,n;
		
		System.out.print("Banyak deret = ");
		n = input.nextInt();
		
		int arr[] = new int[n];
		
		for (i=0 ; i < arr.length; i++) {
			arr[i] = deret7*2;
			deret7 = deret7*2;
			System.out.print(arr[i] + " ");
		}
	}
	
	/////////////////////////////////////////////////
	
	public static void resolve8() {
		Scanner input = new Scanner(System.in);
	
		int i,deret8=1,n;
	
		System.out.print("Banyak deret = ");
		n = input.nextInt();
		
		int arr[] = new int[n];
		
		for (i=0 ; i < arr.length; i++) {
			deret8 = deret8*3;
			arr [i] = deret8;
			System.out.print(arr[i] + " ");
		}
	}
	

	/////////////////////////////////////////////////
	
	public static void resolve9() {
		Scanner input = new Scanner(System.in);
		
		int i,deret9=1,n;
		
		System.out.print("Banyak deret = ");
		n = input.nextInt();
		
		String arr[] = new String[n];
		
		for (i=1 ; i <=n; i++) {
			
			if (i%3==0) {
			
				arr[i - 1] = "*";
			
			}
			else {
				
				deret9 = deret9*4;
				arr [i - 1] = String.valueOf(deret9);
			}
			
			System.out.print(arr[i - 1] + " ");
		}
	}
	
	/////////////////////////////////////////////////
		
	public static void resolve10() {
		Scanner input = new Scanner(System.in);
	
		int i,deret10=1,n;
	
		System.out.print("Banyak deret = ");
		n = input.nextInt();
		
		String arr[] = new String[n];
		
		for (i=1 ; i <=n; i++) {
			
			deret10 = deret10*3;
			
			if (i%4==0) {
				
				arr[i - 1] = "XXX";
			
			}
			else {
			
				arr[i - 1] = String.valueOf(deret10);
			
			}
			
			System.out.print(arr[i - 1] + " ");
		}
	}
}
