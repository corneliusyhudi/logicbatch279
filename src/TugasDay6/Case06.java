package TugasDay6;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

public class Case06 {
	
	public static void resolve1() {
		
		Scanner input = new Scanner(System.in);
		int a = 0, b = 0, jumlah = 0;
		
		System.out.print("a = ");
		a = input.nextInt();
		System.out.print("b = ");
		b = input.nextInt();
	
		jumlah = a + b;
		
		System.out.print(jumlah);
	}
		
	//////////////////////////////////////////////////
	
	public static void resolve2() {
		
		Scanner input = new Scanner(System.in);
		
		String waktu = "";
		String formatWaktuBaru = "";
		
		System.out.print("Time : ");
		waktu = input.nextLine();
		
		String[] formatWaktu = waktu.split(":"); // input jadi [11, 12, 13PM]
		
		int jam = Integer.parseInt(formatWaktu[0]);
		String menit = formatWaktu[1];
		String detik = formatWaktu[2].substring(0,2); // isinya 13PM
		String periodeWaktu = formatWaktu[2].substring(2,4);
		
//		System.out.println();
//		System.out.println(detik);
//		System.out.print(periodeWaktu);
		
		if((jam >= 0 && jam < 12) && (periodeWaktu.equalsIgnoreCase("AM"))) {
			formatWaktuBaru = String.format("%02d", jam) + ":" + menit + ":" + detik; 
		}
		else if((jam == 12) && (periodeWaktu.equalsIgnoreCase("AM"))) {
			formatWaktuBaru = "00" + ":" + menit + ":" + detik; 
		}
		else if ((jam >= 0 && jam < 12) && (periodeWaktu.equalsIgnoreCase("PM"))) {
			formatWaktuBaru = (12 + jam)  + ":" + menit +  ":" + detik; 
		}
		else if((jam == 12) && (periodeWaktu.equalsIgnoreCase("PM"))) {
			formatWaktuBaru = jam + ":" + menit + ":" + detik; 
		}
		
		System.out.print("hasil convert waktu : " + formatWaktuBaru);
		
		
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve3() {
		
		Scanner input = new Scanner(System.in);
		int sum = 0, i = 0, panjang = 0;
		
		
		System.out.print("Input panjang Array =  ");
		panjang = input.nextInt();
		int[] arr = new int[panjang];
		
		System.out.print("Input isi Array = ");
		for (i=0; i < panjang; i++) {
		
			arr[i] = input.nextInt();
			
			sum += arr[i];
		
		}
		
		System.out.print("Result Sum = " + sum);
		
		
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve4() {
		
		Scanner input = new Scanner(System.in);
		
		int i,j;
		int matrix;
		
		
		System.out.print("Input panjang Matrix = ");
		
		matrix = input.nextInt();
		int isiMatrix[][] = new int[matrix][matrix];
		int sumDiagonal1 = 0;
		int sumDiagonal2 = 0;
		int selisihDiagonal = 0;
		
		System.out.println("Isi Matrix : ");
		for (i=0; i<isiMatrix.length; i++) {
			for (j=0; j<isiMatrix.length; j++) {
				
				isiMatrix[i][j] = input.nextInt(); 
				
			}
			
		}
		
		System.out.println();
		System.out.println("Output isi Matrix : ");
		
		for (i=0; i<isiMatrix.length; i++) {
			for (j=0; j<isiMatrix.length; j++) {
				
				System.out.print(isiMatrix[i][j] + "\t");
			}
			System.out.println();
			
		}	
		
		for (i=0; i<isiMatrix.length; i++) {
			for (j=0; j<isiMatrix.length; j++) {
				
				if (i==j){
					sumDiagonal1 += isiMatrix[i][j];
				}
				
				if(i+j == (isiMatrix.length - 1)) {
					sumDiagonal2 += isiMatrix[i][j];
				}
				
			}
			
		}
		
		// Yang ini cara panjang nya tapi sama aja
		
//		for (i=0; i<isiMatrix.length; i++) {
//			for (j=0; j<isiMatrix.length; j++) {
//				
//				if (i==j){
//					sumDiagonal1 += isiMatrix[i][j];
//				}
//				
//			}
//			
//		}
		
		selisihDiagonal = Math.abs(sumDiagonal1 - sumDiagonal2);
		
		System.out.println();
		System.out.println("Jumlah diagonal 1 = " + sumDiagonal1);
		System.out.println("Jumlah diagonal 2 = " + sumDiagonal2);
		System.out.print("Selisih Diagonal = " + selisihDiagonal);
	
		
		
	}
			
	//////////////////////////////////////////////////
	
	public static void resolve5() {
		
		Scanner input = new Scanner(System.in);
		int positif = 0, negatif = 0, zero = 0, i = 0, panjang = 0;
		
		System.out.print("Input panjang Array =  ");
		panjang = input.nextInt();
		int[] angka = new int[panjang];

		System.out.print("Input isi Array = ");
		
		for (i=0; i < panjang; i++) {
		
			angka[i] = input.nextInt();
			
			if (angka[i] < 0) {
				negatif += 1;
			}
			else if (angka[i] > 0) {
				positif += 1;
			}
			else if (angka[i] == 0) {
				zero += 1;
			}
		}
		
		double hasilPositif = (double)positif/panjang;
		double hasilNegatif = (double)negatif/panjang;
		double hasilNol = (double)zero/panjang;
		
		NumberFormat formatter= new DecimalFormat("#0.00000");         
		
		System.out.println("Rata-rata banyak bilangan positif = " + formatter.format(hasilPositif));
		System.out.println("Rata-rata banyak bilangan negatif = " + formatter.format(hasilNegatif));
		System.out.print("Rata-rata banyak bilangan nol = " + formatter.format(hasilNol));
		
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve6() {
		
		Scanner input = new Scanner(System.in);
		
		int i,j,n;
		
		System.out.print("Masukkan nilai N = ");
		n = input.nextInt();
		
		for(i=0; i<n; i++){
			for(j=0; j<n-i; j++){ 
			     
				System.out.print(" ");
			}
			for (j=0; j<=i; j++){
			   System.out.print("#");
			}
		
			System.out.println();
		}
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve7() {
		
		Scanner input = new Scanner(System.in);
		
		int i = 0, j = 0, min = 0, max = 0, sum = 0; 
		int panjangAngka;
		
		System.out.print("Input panjang Index angka : ");
		panjangAngka = input.nextInt();
		
		int angka[] = new int[panjangAngka];
		
		System.out.print("Input Angka = ");
		
		for (i=0; i<panjangAngka; i++) {
			angka[i] = input.nextInt();	
		}
		
		// sorting angka (ini cuman hiasan aja)
		
//		int temp = 0;
//		for (i=0;i<=panjangAngka-2;i++){
//			int minAngka=i;
//			for (j=i;j<=panjangAngka-1;j++) {
//				if (angka[j]<angka[minAngka]) {
//					minAngka=j;
//				}
//			}
//			
//		temp = angka[minAngka];
//		angka[minAngka]=angka[i];
//		angka[i]=temp;
//		}
//		
//		System.out.print("Hasil Sorting = ");
//		for(i=0; i < panjangAngka; i++) {
//			System.out.print(angka[i] + " ");
//		}
		
		System.out.println();
		
		// mencari min, max, dan sum
		// menggunakan sequential search (gausah di sorting langsung bisa dapet)
		min = angka[0]; // titik start searching semua mulai dari index 0
		max = min; // titik start searching semua mulai dari index 0
		sum = min; // titik start searching semua mulai dari index 0
		
		for (i=1; i < panjangAngka; i++) {
			sum += angka[i];
			if (angka[i] < min) {
				min = angka[i];
			}
			if (angka[i] > max) {
				max = angka[i];
			}
		}
		
		System.out.println("Nilai Max = " + (sum - max) + "\n" + "Nilai Min = " + (sum - min));
		
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve8() {
		
		Scanner input = new Scanner(System.in);
		
		int i = 0, j = 0, sum = 0;
		int panjangAngka;
		
		System.out.print("Input panjang Index : ");
		panjangAngka = input.nextInt();
		
		int angka[] = new int[panjangAngka];
		
		System.out.print("Input ukuran lilin = ");
		
		for (i=0; i<panjangAngka; i++) {
			angka[i] = input.nextInt();	
		}
		
		int max = angka[0];
		
		for (i=1; i < panjangAngka; i++) {
			if (angka[i] > max) {
				max = angka[i];
			}
		}
		
		for(i=0; i < panjangAngka; i++) {
			if (max == angka[i]) {
				sum++;
			}
		}
		
		System.out.print("Lilin tertinggi ada = " + sum);
	
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve9() {
		
		Scanner input = new Scanner(System.in);
		
		int banyakIndex = 0, i,j;
		long jumlah = 0;
		
		System.out.print("Input banyak index = ");
		banyakIndex = input.nextInt();
		long [] angka = new long[banyakIndex];
		
		System.out.print("Input angka = ");
		for(i=0; i < banyakIndex; i++) {
			angka[i] = input.nextLong();	
		}
		
		// Hasil
		for(i=0; i < banyakIndex; i++) {
			jumlah += angka[i];	
		}
		
		System.out.print("Hasil jumlah = " + jumlah);
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve10() {
		
		Scanner input = new Scanner(System.in);
		int i,j,x, scoreAlice = 0, scoreBob = 0;
		
		System.out.print("Banyak score masing-masing = ");
		int banyak = input.nextInt();
		int[] alice = new int[banyak];
		int[] bob = new int[banyak];
		
		System.out.print("Alice's Score : ");
		
		for(i=0; i<banyak; i++) {
			alice[i] = input.nextInt();
		}
		
		System.out.print("Bob's Score : ");
		
		for(i=0; i<banyak; i++) {
			bob[i] = input.nextInt();
		}
		
		for(i=0; i<banyak; i++) {
			if(alice[i] > bob[i]) {
				scoreAlice ++;
			}
			else if (alice[i] < bob[i]) {
				scoreBob++;
			}
		}
		
		System.out.println("Nilai tertinggi Alice ada = " + scoreAlice);
		System.out.print("Nilai tertinggi Bob ada = " + scoreBob);
		
	}


}


	
	
	
	
	