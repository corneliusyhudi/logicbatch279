package day4;

import java.util.Arrays;

public class LatManipulasiString {
	
	public static void main ( String[] args ) {
	
		String nama = " Satria Ramadhana Putra";
		
		// Pecah kalimat menjadi array. Index 0 - 2
		String[] pecahKalimat = nama.split(" ");
		
		// Pecah Kaata menjadi char dengan index sesuai kata
		char[] pecahKataPertama = pecahKalimat[0].toCharArray();
		pecahKataPertama[5] = 'o';
		
		char[] pecahKataKedua = pecahKalimat[1].toCharArray();
		pecahKataKedua[5] = 'i';
		
		char[] pecahKataKetiga = pecahKalimat[2].toCharArray();
		pecahKataKetiga[5] = 'i';
		
		// int s = pecahKata[0]; // Menampilkan ASCII char
		
		System.out.println(Arrays.toString(pecahKataPertama));
		System.out.println(Arrays.toString(pecahKataKedua));
		System.out.println(Arrays.toString(pecahKataKetiga));
	}

}
