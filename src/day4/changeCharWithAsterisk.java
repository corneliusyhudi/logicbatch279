package day4;
import java.util.*;

public class changeCharWithAsterisk {
	
	public static void main (String[] args) {
		
		Scanner input = new Scanner(System.in);
		String kalimat = "";
		String kalimatFinal = "";
		int i, j, x;
		char [] pecahKata = null;
		
		System.out.print("Kalimat: " );
		kalimat = input.nextLine();
		
		String[] pecahKalimat = kalimat.split(" ");
		
		System.out.println();
		System.out.println("Output 1 : ");
		
		for (i=0; i < pecahKalimat.length; i++) {
			
			pecahKata = pecahKalimat[i].toCharArray();
//			System.out.print(Arrays.toString(pecahKata) + " ");
			
			for (j=0; j < pecahKata.length; j++) {
				
				// tengah jadi bintang
				if ( j > 0 && j != pecahKata.length-1 ) {
					
					pecahKata[j] = '*';
					System.out.print(pecahKata[j]);
				}
				else {
					pecahKata[j] = pecahKata[j];
					System.out.print(pecahKata[j]);
				}
			
				
			}
			System.out.print(" ");
			
		}
		
		System.out.println();
		System.out.println();
		System.out.println("Output 2 : ");
		
		for (i=0; i < pecahKalimat.length; i++) {
			
			pecahKata = pecahKalimat[i].toCharArray();
//			System.out.print(Arrays.toString(pecahKata) + " ");
			
			for (j=0; j < pecahKata.length; j++) {
				
				// tengah jadi bintang
				if ( j > 0 && j != pecahKata.length-1 ) {
					
					pecahKata[j] = pecahKata[j];
					System.out.print(pecahKata[j]);
				}
				else {
					
					pecahKata[j] = '*';
					System.out.print(pecahKata[j]);
				}
			
				
			}
			
			System.out.print(" ");
			
		}
		
	}
		
}

