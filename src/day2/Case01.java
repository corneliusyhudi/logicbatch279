package day2;

import java.util.Scanner;

public class Case01 {

	public static void resolve1() {
		System.out.print("Masukkan Nama Anda: ");
		
		Scanner input = new Scanner(System.in);
		String nama = input.nextLine();
		
		System.out.println("Nama Anda adalah : " + nama);		
		
		System.out.print("Masukkan Nilai X = ");
		int x = input.nextInt();
		
		System.out.print("Masukkan Nilai Y = ");
		int y = input.nextInt();
		
		System.out.print("Hasil dari Nilai X + Y = " + (x+y));
		System.out.print("Hasil dari Nilai X - Y = "+ (x-y));
		
		
//		String nilaiA = input.nextLine();
//		String nilaiB = input.nextLine();
//		
//		System.out.println("Hasil A + B = " + (Integer.parseInt(nilaiA)+Integer.parseInt(nilaiB)));
	}
	
	public static void resolve2() {
		
		Scanner input = new Scanner(System.in);
		
		int pulsa, point1, point2, point3, totalPoint;
		
		System.out.println("Beli Pulsa : ");
		pulsa = input.nextInt();
		
		if (pulsa < 10000) {
			point1 = 0;
			point2 = 0;
			point3 = 0;
			
			
		} else if(pulsa <= 60000) {
			point1 = 0;
			point2 = (pulsa - 10000)/1000; 
			point3 = 0;
			
			
				
		} else {
			point1 = 0;
			point2 = 20;
			point3 = (pulsa - 30000)/1000*2;
			
		}
		
		
		totalPoint = point1 + point2 + point3;
		
				System.out.println("Total Point = " + totalPoint );
			
		
	}
	
	public static void resolve3() {
		
		Scanner input = new Scanner(System.in);
		
		int belanja = 0, jarak = 0, ongkir = 0;
		double hargaDisc = 0.0;
		int disc = 0;
		String promo = "";
		
		System.out.println("Belanja = ");
		belanja = input.nextInt();
		
		System.out.println("Jarak = ");
		jarak = input.nextInt();
		
		input.nextLine();
		
		System.out.println("Masukkan Promo = ");
		promo = input.nextLine();
		
		// Belanja = 30000
		// Jarak = 4 km
		// Voucher = JKTOVO
		// Kondisi pertama harus ada voucher kalau mau diskon
		if (promo.equals("JKTOVO")) { 
			if(belanja >= 30000) { // cek apakah belanja nya >= 30k
				disc = 40;
			}
			else {
				disc = 0; // jika masukin voucher tapi belanja < 30k
			}
			
		} else {
			disc = 0; // gak masukin voucher (kondisi >= 30k atau < 30k)
		}
		
		// Jika tidak ada minimal jarak 5k
		// ongkir = jarak*1000;
		
		if (jarak <= 5) {
			ongkir = 5000;
		}
		else {
			ongkir = jarak*1000;
		}
		
		hargaDisc = (double)belanja*disc/100; // 30000*40/100 = 12000
		
		if (hargaDisc > 30000) { // maks 30k, kalau lebih dari 30k maka hargaDisc = 30k
			hargaDisc = 30000;
		}
		
		int totalBelanja = (int)(belanja - hargaDisc + ongkir); // 30000-12000+5000 = 23000;
		
		System.out.println("Belanja = " + belanja);
		System.out.println("Diskon 40% = " + hargaDisc);
		System.out.println("Ongkir = " + ongkir);
		System.out.println("Total Belanja = " + totalBelanja); //
		
			
	}
	
	public static void resolve4() {
		
		Scanner input = new Scanner(System.in);
		
		int belanja, ongkir, order, diskonOngkir, totalDiskonOngkir, diskonBelanja, totalBelanja;
		
		System.out.println("Belanja = ");
		belanja = input.nextInt();
		
		System.out.println("Ongkir = ");
		ongkir = input.nextInt();
		
		order = belanja + ongkir;
		
		if (order >= 100000) {
			diskonOngkir = 10000;
			diskonBelanja = 20000;
		}
		else if(order >= 50000) {
			diskonOngkir = 10000;
			diskonBelanja = 10000;
		}
		else if (order >= 30000){
			diskonOngkir = 5000;
			diskonBelanja = 5000;
		}
		else {
			diskonOngkir = 0;
			diskonBelanja = 0;
		}
		
		totalBelanja = order - diskonOngkir - diskonBelanja; 
		
		System.out.println("Belanja = " + belanja);
		System.out.println("Ongkir = " + ongkir);
		System.out.println("Diskon Ongkir = " + diskonOngkir);
		System.out.println("Diskon Belanja = " + diskonBelanja);
		System.out.println("Total Belanja = " + totalBelanja);
		
	}
	
	public static void resolve5() {
		
		Scanner input = new Scanner(System.in);
		int pulsa, point = 0; 
		
		System.out.println("Pulsa : ");
		pulsa = input.nextInt();
		
		if (pulsa >= 100000) {
			point = 800;
		}
		else if (pulsa >= 50000) {
			point = 400;
		}
		else if (pulsa >= 25000) {
			point = 200;
		}
		else if (pulsa >= 10000) {
			point = 80;
		}
		
		System.out.println("Pulsa : " + pulsa);
		System.out.println("Point : " + point);
	}
	

}
