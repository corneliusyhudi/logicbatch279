package TugasDay3;

import java.util.*;

public class Case03 {
	
	
	public static void resolve1() {
		
		Scanner input = new Scanner(System.in);
		int i,j,n1,n2,operasi = 1;
		
		System.out.print("Masukkan banyak n1 = ");
		n1 = input.nextInt();
		System.out.print("Masukkan nilai n2 = ");
		n2 = input.nextInt();
		
		int[][] arr = new int[2][n1];
		
		for(i=0; i<arr.length; i++) {
			for(j=0; j<arr[i].length; j++) {
				
				if (i==0) {
					arr[i][j] = j;
					System.out.print(arr[i][j] + " ");
				}
				else {
					arr[i][j] = operasi;
					operasi = operasi*n2;
					System.out.print(arr[i][j] + " ");
				}
				
			}
			
			System.out.println();
		}

	}
	
	//////////////////////////////////////////////////
	
	public static void resolve2() {
		
		Scanner input = new Scanner(System.in);
		int i,j,n1,n2,operasi = 1;
		
		System.out.print("Masukkan banyak n1 = ");
		n1 = input.nextInt();
		System.out.print("Masukkan nilai n2 = ");
		n2 = input.nextInt();
		
		int[][] arr = new int[2][n1];
		
		for(i=0; i<arr.length; i++) {
			for(j=1; j<=arr[i].length; j++) {
				
				if (i==0) {
					arr[i][j-1] = j-1;
					System.out.print(arr[i][j-1] + " ");
				}
				else {
					if ( j > 2 && j%3 == 0) {
						
						arr[i][j-1] = (-operasi);
						System.out.print(arr[i][j-1] + " ");
						operasi = operasi * (n2);
						
					}
					else if (j>=1){
						
						arr[i][j-1] = operasi;
						System.out.print(arr[i][j-1] + " ");
						operasi = operasi * n2;
						
					
					}
				}
				
			}
		
			System.out.println();
		}
	}
	
//////////////////////////////////////////////////
	
	public static void resolve3() {
	
		Scanner input = new Scanner(System.in);
		int i,j,n1,n2;
		int m;
		
		System.out.print("Masukkan banyak n1 = ");
		n1 = input.nextInt();
		System.out.print("Masukkan nilai n2 = ");
		n2 = input.nextInt();
		
		int[][] arr = new int[2][n1];
		
		// Mencari Median
		if (n1 % 2 == 1) {
			  m = n1 / 2;
		} 
		else {
			  m = (n1 - 1)/ 2;
		}
		
		for(i=0; i<arr.length; i++) {
			for(j=1; j<=arr[i].length; j++) {
				
				if (i == 0) {
					arr[i][j-1] = j-1;
					System.out.print(arr[i][j-1] + " ");
				}
				else {
					
					if (n1 % 2 != 0) {
						
						if(j <= m) {
							arr[i][j-1] = n2;
							System.out.print(arr[i][j-1] + " ");
							n2 = n2*2;
						}
						else {
							
							arr[i][j-1] = n2;
							System.out.print(arr[i][j-1] + " ");
							n2 = n2/2;
						
						}
					}
					
					else {
						
						if(j <= m) {
							arr[i][j-1] = n2;
							System.out.print(arr[i][j-1] + " ");
							n2 = n2*2;
						}
						
						else if (j >= m+1 && j <= m+2) {
							arr[i][j-1] = n2;
							System.out.print(arr[i][j-1] + " ");
						}
						
						else {
							
							n2 = n2/2;
							arr[i][j-1] = n2;
							System.out.print(arr[i][j-1] + " ");
						
						}
						
					}
					
					
				}
		
			}
			
			System.out.println();
		}
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve4() {
		
		Scanner input = new Scanner(System.in);
		int i,j,n1,n2,operasi1 = 1;
		
		System.out.print("Masukkan banyak n1 = ");
		n1 = input.nextInt();
		System.out.print("Masukkan nilai n2 = ");
		n2 = input.nextInt();
		
		int  operasi2 = n2;
		
		int[][] arr = new int[2][n1];
		 
		for(i=0; i<arr.length; i++) {
			for(j=1; j<=arr[i].length; j++) {
				
				if (i == 0) {
					arr[i][j-1] = j-1;
					System.out.print(arr[i][j-1] + " ");
				}
				else {
					
					if(j%2 != 0) {
						arr[i][j-1] = operasi1;
						System.out.print(arr[i][j-1] + " ");
						operasi1 = operasi1+1;
						
					}
					else {
						arr[i][j-1] = operasi2;
						System.out.print(arr[i][j-1] + " ");
						operasi2 = operasi2+n2;
						
					}
					
					
				}
		
			}
			
			System.out.println();
		}
	}
	
	//////////////////////////////////////////////////	
	
	public static void resolve5() {
		
		Scanner input = new Scanner(System.in);
		int i,j,n1,operasi = 0;
		
		System.out.print("Masukkan banyak n1 = ");
		n1 = input.nextInt();
		
		int[][] arr = new int[3][n1];
		 
		for(i=0; i<arr.length; i++) {
			for(j=1; j<=arr[i].length; j++) {
				
				arr[i][j-1] = operasi;
				System.out.print(arr[i][j-1] + " ");
				operasi = operasi + 1;	
				
			}
			System.out.println();
		}
	}
	
	//////////////////////////////////////////////////	
	
	public static void resolve6() {
		
		Scanner input = new Scanner(System.in);
		int i,j,n1,operasi = 1;
		
		System.out.print("Masukkan banyak n1 = ");
		n1 = input.nextInt();
		
		int[][] arr = new int[3][n1];
		 
		for(i=0; i<arr.length; i++) {
			for(j=1; j<=arr[i].length; j++) {
				
				if (i == 0) {
					
					arr[i][j-1] = j - 1;
					System.out.print(arr[i][j-1] + " ");
				
				}
				
				else if (i == 1) {
					
					arr[i][j-1] = operasi;
					System.out.print(arr[i][j-1] + " ");
					operasi = operasi * n1;
				
				}
				
				else {
					
					arr[i][j-1] = arr[0][j-1] + arr[1][j-1];
					System.out.print(arr[i][j-1] + " ");
				}
				
			}
			
			System.out.println();
		}
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve7() {
		
		Scanner input = new Scanner(System.in);
		int i,j,n1,operasi = 0;
		
		System.out.print("Masukkan banyak n1 = ");
		n1 = input.nextInt();
		
		int[][] arr = new int[3][n1];
		 
		for(i=0; i<arr.length; i++) {
			for(j=1; j<=arr[i].length; j++) {
					
					arr[i][j-1] = operasi;
					System.out.print(arr[i][j-1] + " ");
					operasi = operasi + 1;
			}
			
			System.out.println();
		}
	}	
	
	//////////////////////////////////////////////////
	
	public static void resolve8() {
		
		Scanner input = new Scanner(System.in);
		int i,j,n1,operasi = 0;
		
		System.out.print("Masukkan banyak n1 = ");
		n1 = input.nextInt();
		
		int[][] arr = new int[3][n1];
		 
		for(i=0; i<arr.length; i++) {
			for(j=1; j<=arr[i].length; j++) {
				
				if (i == 0) {
					
					arr[i][j-1] = j - 1;
					System.out.print(arr[i][j-1] + " ");
				
				}
				
				else if (i == 1) {
					
					arr[i][j-1] = operasi;
					System.out.print(arr[i][j-1] + " ");
					operasi = operasi + 2;
				
				}
				
				else {
					
					arr[i][j-1] = arr[0][j-1] + arr[1][j-1];
					System.out.print(arr[i][j-1] + " ");
				}
				
			}
			
			System.out.println();
		}
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve9() {
		
		Scanner input = new Scanner(System.in);
		int i,j,n1,n2,operasi = 0;
		
		System.out.print("Masukkan banyak n1 = ");
		n1 = input.nextInt();
		System.out.print("Masukkan nilai n2 = ");
		n2 = input.nextInt();
		
		int[][] arr = new int[3][n1];
		 
		for(i=0; i<arr.length; i++) {
			for(j=1; j<=arr[i].length; j++) {
					
				if (i == 0) {
					
					arr[i][j-1] = j - 1;
					System.out.print(arr[i][j-1] + " ");
				
				}
				
				else if (i == 1) {
					
					arr[i][j-1] = operasi;
					System.out.print(arr[i][j-1] + " ");
					operasi = operasi + n2;
					
				}
				
				else {
					
					arr[i][j-1] = operasi - n2;
					System.out.print(arr[i][j-1] + " ");
					operasi = operasi - n2;
					
				}
			}
			
			System.out.println();
		}
	}
	
	//////////////////////////////////////////////////	
	
	public static void resolve10() {
		
		Scanner input = new Scanner(System.in);
		int i,j,n1,n2,operasi = 0;
		
		System.out.print("Masukkan banyak n1 = ");
		n1 = input.nextInt();
		System.out.print("Masukkan nilai n2 = ");
		n2 = input.nextInt();
		
		int[][] arr = new int[3][n1];
		 
		for(i=0; i<arr.length; i++) {
			for(j=1; j<=arr[i].length; j++) {
					
				if (i == 0) {
					
					arr[i][j-1] = j - 1;
					System.out.print(arr[i][j-1] + " ");
				
				}
				
				else if (i == 1) {
					
					arr[i][j-1] = operasi;
					System.out.print(arr[i][j-1] + " ");
					operasi = operasi + n2;
					
				}
				
				else {
					
					arr[i][j-1] = arr[0][j-1] + arr[1][j-1];
					System.out.print(arr[i][j-1] + " ");
					
				}
			}
			
			System.out.println();
		}
	}
	
}
















