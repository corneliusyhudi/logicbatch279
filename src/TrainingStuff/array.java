package TrainingStuff;

import java.util.*;

public class array {
	public static void main(String[] args) {
		
		int i;
		Scanner sc =  new Scanner(System.in);
		
		System.out.print("Banyak Karyawan : ");
		int n = sc.nextInt();
		String[] nama = new String[n];
		
		for (i=0; i<n; i++) {
			System.out.print("Nama Karyawan " + (i+1) + " :");
			nama[i] = sc.next();
		}
		sc.close();
		
		
		System.out.println("Nama Karyawan yang diinput : ");
		for (i=0; i<n; i++) {
			System.out.println((i+1) + ". " + nama[i]);
		}
		
	}
}
