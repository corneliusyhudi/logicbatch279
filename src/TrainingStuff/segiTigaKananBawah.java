package TrainingStuff;

import java.util.Scanner;

public class segiTigaKananBawah {
	public static void main(String[] args) {
		
		int i,j,n;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Input banyak bintang : ");
		n = sc.nextInt();
	
		for (i=0; i<n; i++) {
			for(j=0; j<n; j++) {
				if (i == n-1 || i+j == n-1 || j == n-1) {
					System.out.print("*");
				}
				else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
}
