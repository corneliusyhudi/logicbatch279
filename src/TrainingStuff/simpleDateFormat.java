package TrainingStuff;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class simpleDateFormat {
	
	public static void main (String[] args) {
		
		SimpleDateFormat formatWaktu = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		
		Scanner input = new Scanner(System.in);
		
		Date waktuMasuk = new Date();
		Date waktuKeluar = new Date();
		int biayaParkir = 0, i;
		
		System.out.println("Masukkan waktu masuk parkiran (dd/MM/yy HH:mm:ss) = ");
		String inputMasukParkir = input.nextLine();
		
		System.out.println("Masukkan waktu keluar parkiran (dd/MM/yy HH:mm:ss) = ");
		String inputKeluarParkir = input.nextLine();
		
		try {
			
			waktuMasuk = formatWaktu.parse(inputMasukParkir);
			waktuKeluar = formatWaktu.parse(inputKeluarParkir);
			
		} catch(ParseException e){
			
			System.out.println("Format waktu tidak sesuai!\n" + e);
			
		}
		
		long diff = waktuKeluar.getTime() - waktuMasuk.getTime();
		long diffDetik = diff / 1000 % 60;
		long diffMenit = diff / (60*1000) % 60;
		long diffJam = diff / (60*60*1000) % 60;
		
		if(diffDetik > 0 || diffMenit >0) {
			biayaParkir += 3000;
			
			for (i = 0; i < diffJam; i++) {
				biayaParkir += 3000;
			}
			
		} else {
			biayaParkir += diffJam * 3000;
		}
		
		if (diff > 0) {
			System.out.println("Biaya Parkir : Rp" + biayaParkir);
		} else if (diff < 0){
			System.out.println("Waktu keluar lebih dari waktu masuk parkiran");
		} else if (diff == 0) {
			System.out.println("Biaya parkir gratis");
		}
		
	}
	
}
