package TugasDay8;

import java.util.*;
import java.util.regex.*;

public class Case08 {
	
	public static void resolve1() {
		
		Scanner input = new Scanner(System.in);
		int i = 0;
		
		
		String kalimat = "";
		System.out.print("Input Kalimat = ");
		kalimat = input.nextLine();
		
		char [] kalimatChar = kalimat.toCharArray();
		int cek = 0;
		boolean upperOrLower = false;
		
		if(Character.isLowerCase(kalimatChar[0])) {
			cek++;
		} else {
			cek++;
		}
		
		for (i = 1; i < kalimat.length(); i++) {
			 
			if(Character.isLowerCase(kalimatChar[i]) == upperOrLower) {
				 cek++;
			}
		}
		
		System.out.print("Banyak Kata = " + cek);
	}
		
	//////////////////////////////////////////////////
	
	public static void resolve2() {
		
		Scanner input = new Scanner(System.in);
		String pass= "";
		
		int i = 0;
		int cek = 4, syarat = 0, banyakCharMinimal = 6;
		int panjangPass = 0;
		
		Pattern upperCase = Pattern.compile("[A-Z]+");
		Pattern lowerCase = Pattern.compile("[a-z]+");
		Pattern angka = Pattern.compile("[0-9]+");
		Pattern specialCharacter = Pattern.compile("[!@#$%^&*()\\-+]");
		
		
		do {
		System.out.print("Panjang password : ");
		panjangPass = input.nextInt();
		
		System.out.print("Input your password : ");
		pass = input.next();
		
			if (pass.length() > panjangPass) {
				System.out.println();
				System.out.println("Password melebihi index!\n");
			}
			else if(pass.length() < panjangPass) {
				System.out.println();
				System.out.println("Password kurang dari index!\n");
			}
		
		}while(pass.length() != panjangPass);
			
	
		if(upperCase.matcher(pass).find()){
			syarat++;
		}
		if(lowerCase.matcher(pass).find()){
			syarat++;
		}
		if(angka.matcher(pass).find()){
			syarat++;
		}
		if(specialCharacter.matcher(pass).find()){
			syarat++;
		}
			
		int result = Math.max(banyakCharMinimal-panjangPass, cek-syarat);
//		System.out.println(syarat);
		System.out.println(result);
		
		
	}

	//////////////////////////////////////////////////
	
	public static void resolve3() {
		
		Scanner input = new Scanner(System.in);
		int i, j, n, rotate;
		String alfabet = "abcdefghijklmnopqrstuvwxyz";
		String kalimat = "";
		
		
		do {
		
		System.out.print("Input banyak kalimat : ");
		n = input.nextInt();
		
		System.out.print("Input kalimat : ");
		kalimat = input.next();
		
		System.out.print("Rotate : ");
		rotate = input.nextInt();
		
		if (kalimat.length() != n) {
			System.out.println();
			System.out.println("Kalimat tidak sesuai dengan range!");
		}
		
		}while (kalimat.length() != n);
		
		char[] temp = new char[kalimat.length()];
		char[] kalimatChar = kalimat.toCharArray();
		char[] alfabetChar = alfabet.toCharArray();
		
		System.out.println(Arrays.toString(alfabetChar));
		
		for(i=0; i<n; i++) {
			
			if(Character.isAlphabetic(kalimatChar[i])) {
			
				for(j=0; j<alfabet.length(); j++) {
				
					if(Character.isLowerCase(kalimatChar[i])) {
						if(kalimatChar[i] == alfabetChar[j]) {
							temp[i] = alfabetChar[(rotate+j)%alfabetChar.length];
						
						}
						
					}
					
					else if (Character.isUpperCase(kalimatChar[i])) {
						if(Character.toLowerCase(kalimatChar[i]) == alfabetChar[j]) {
							temp[i] =  Character.toUpperCase(alfabetChar[(rotate+j)%alfabetChar.length]);
							
						}
					}
					
				}
			}else {
				temp[i] = kalimatChar[i];
			}
			
			System.out.print(temp[i]);
		}
		
		
		
		
		
	}
		
	//////////////////////////////////////////////////
	
	public static void resolve4() {
		
		Scanner input = new Scanner(System.in);
		int cek = 0, i;
		
		String sos = "";
		
		System.out.print("Input Code = ");
		sos = input.nextLine().toUpperCase();
//		char [] pecahSOS = new char[sos.length()];
		
		
		// Misal input SOSSOSSOS = salah 0
		// Misal input SOSPSPAAS = salah 2
		// Misal input ABCPQSSQP = salah 3
		
		// ABC PQS cek = 2, SQP cek = 2, 3
		for (i=0; i<sos.length(); i++) {
		
			// salah
			
//			if (sos.charAt(i) != 'S'){ 
//				cek++; 
//				continue;
//			}
//			
//			if (sos.charAt(i+1) != 'O') {
//				cek++;
//				if (sos.charAt(i) != 'S'){ 
//					cek--; 
//					continue;
//				}
//				
//			}
//			
//			if (sos.charAt(i+2) != 'S') {  
//				cek++;
//				if (sos.charAt(i+1) != 'O'){ 
//					cek --;
//					continue;
//				}
//				
//			}
			
	         if(i % 3 == 1)
	            {
	                if(sos.charAt(i) != 'O')
	                {
	                    cek++;
	                }
	            }
	            else
	            {
	                if(sos.charAt(i) != 'S')
	                {
	                    cek++;
	                }
	            }
			
			
		}
		
		System.out.print("Kode yang salah ada = " + cek);
		

	}
	
	//////////////////////////////////////////////////
	
	public static void resolve5() {
		
		Scanner input = new Scanner(System.in);
		int i, n;
		String text = "";
		
		Pattern cek = Pattern.compile(".*h.*a.*c.*k.*e.*r.*r.*a.*n.*k.*");
		
		System.out.print("Banyak String = ");
		n = input.nextInt();
		
		System.out.println("Input String : ");
		for(i=0; i < n; i++) {
			text = input.next();
			System.out.println(cek.matcher(text).find() ? "YES" : "NO");
		}
	}

	//////////////////////////////////////////////////
	
	public static void resolve6() {
		
		Scanner input = new Scanner(System.in);
		
		String text = "";
		String modifText = "";
		int i;
		
		System.out.print("Input text : ");
		text = input.nextLine();
		
		modifText = text.replaceAll(" ","").toLowerCase();
//		System.out.print(modifText);
		
		Set<Character> ts= new TreeSet<>();
		
		for(i=0; i < modifText.length(); i++) {
			ts.add(modifText.charAt(i));
		}
		
		System.out.println();
		if(ts.size()==26) {
			System.out.print("Pangram");
		}
		else {
			System.out.print("Not Pangram");
		}
		
	}
	
	//////////////////////////////////////////////////
	
	public static void pisahAngka(String angka) {
		int i;
		
		String pisahAngka2 = "";
		String hasilPisahAngka = "";
		boolean adalahBenar = false;
		
		// 1 2 3 4 <=> sampe yang tengah aja jadi >> 1 2
//		System.out.print(angka.length());
		for (i=1; i <=angka.length() / 2; i++) {
			pisahAngka2 = angka.substring(0,i); // pisahAngka2 = 1
			Long nomor = Long.parseLong(pisahAngka2);// nomor = 1
			hasilPisahAngka = pisahAngka2; // hasilPisahAngka = 1
			
			while(hasilPisahAngka.length() < angka.length()) { // 1<=4
				hasilPisahAngka += Long.toString(++nomor); // hasilPisahAngka = 1,2,3,4
			}
			if (angka.equals(hasilPisahAngka)) {
				adalahBenar = true;
				break;
			}
		}
		System.out.println(angka);
		System.out.println(hasilPisahAngka);
		System.out.println(adalahBenar ? "YES " + pisahAngka2 : "NO");
		
	}
	
	
	public static void resolve7() {
		
		Scanner input = new Scanner(System.in);
		int i,n;
		String angka = "";
		
		System.out.print("Input banyak baris angka = ");
		n = input.nextInt();
		
		System.out.print("Input angka = ");
		for(i=0; i<n; i++) {
			angka = input.next();
			pisahAngka(angka);
		}
		
		
		
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve8() {
		
		Scanner input = new Scanner(System.in);
		
		String gem = "";
		int i, n;
		
		// 3 gem <=> abc, cde, efg. dipanggil set nya biar masuk ke list. [set1, set2, set3]
		
		
		List<Set<Character>> groupGem = new ArrayList<Set<Character>>();
		// <> parameter nya berupa objek
		
		System.out.print("Input banyak gemstones = ");
		n = input.nextInt();
		
		System.out.println("Gemstone = ");
		
		for (i=0; i<n; i++) {
			gem = input.next();
		
			Set<Character> set = new HashSet<Character>(26); // Character sebagai objek, dimasukin ke objek
			
			for (char gemChar : gem.toCharArray()) {
				set.add(gemChar); //3 gem udah dimasukin ke set
			}
		
			groupGem.add(set);
		}
		
		Set<Character> wadahSet1 = new HashSet<Character>(26);
		
		wadahSet1 = groupGem.get(0);
		
		for(i=1; i<n; i++) {
			wadahSet1.retainAll(groupGem.get(i));
			
		}
		
		int hitung = wadahSet1.size();
		
		System.out.println("Gem = " + hitung);
		
		
	}

	//////////////////////////////////////////////////
	
	public static void resolve9() {
		
		Scanner input = new Scanner(System.in); 
		
		String text1 = "";
		String text2 = "";
		
		int hapus = 0, i, charToInt = 0, posisi = 0, beda = 0;
		int charToInt2 = 0, posisi2 = 0;
		int[] text1Alfabet = new int[26];
		int[] text2Alfabet = new int[26];
		
		System.out.print("Text 1 = ");
		text1 = input.nextLine();
		
		System.out.print("Text 2 = ");
		text2 = input.nextLine();
		
		for (i=0; i<text1.length(); i++) {
			char currentChar = text1.charAt(i);
			System.out.println(currentChar);
			charToInt = (int)currentChar;
			System.out.println(charToInt);
			posisi = charToInt - 97; // 2
			text1Alfabet[posisi]++; //
			System.out.print(text2Alfabet[posisi]);
		}
		System.out.println();
		for (i=0; i<text2.length(); i++) {
			char currentChar2 = text2.charAt(i);
			System.out.println(currentChar2);
			charToInt2 = (int)currentChar2;
			System.out.println(charToInt2);
			posisi2 = charToInt2 - (int)'a';
			System.out.print(text2Alfabet[posisi2]);
			text2Alfabet[posisi2]++;
		}
		System.out.println();
		System.out.println();
		for (i=0; i<26; i++) {
			System.out.println(text1Alfabet[i]); // 0 0 1 1 1 0 0 0 0 0 ....
//			System.out.println(text2Alfabet[i]); // 1 1 1 0 0 0 0 0 0 0 ....
			beda = Math.abs(text1Alfabet[i] - text2Alfabet[i]);
			hapus += beda;
		}
		
		System.out.print(hapus);
	       
	}
	
	public static void resolve10() {
		
		Scanner input = new Scanner(System.in);
		
		String text1 = "";
		String text2 = "";
		
		int i;
		
		HashSet <Character> text1Character = new HashSet();
		HashSet <Character> text2Character = new HashSet();
		
		System.out.print("Input String 1 = ");
		text1 = input.nextLine();
		System.out.print("Input String 2 = ");
		text2 = input.nextLine();
		
		for(i=0; i<text1.length(); i++) {
			text1Character.add(text1.charAt(i));
		}
//		System.out.print(text1Character);
		for(i=0; i<text2.length(); i++) {
			text2Character.add(text2.charAt(i));
		}
		
//		text1Character.addAll(text2Character); // union
//		System.out.print(text1Character);
		text1Character.retainAll(text2Character); // intersection
		System.out.print(text1Character);
		
		System.out.println( (text1Character.isEmpty()) ? "NO" : "YES" );
	}

	//////////////////////////////////////////////////
}