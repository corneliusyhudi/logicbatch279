package TugasDay4;

import java.util.*;

public class Case04 {
	
	public static void resolve1() {
		
		Scanner input = new Scanner(System.in);
		int i, range;
		int n1 = 1, n2 = 1, sum = 0;
		
		
		System.out.print("Input = ");
		range = input.nextInt();
		
		System.out.print(n1 + " " + n2);
		for (i=2; i < range; i++) {
			sum = n1+n2;
			System.out.print(" " + sum);
			
			n1 = n2;
			n2 = sum;
		}
		
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve2() {
		
		Scanner input = new Scanner(System.in);
		int i, range;
		int n1 = 1, n2 = 1, n3 = 1, sum = 0;
		
		
		System.out.print("Input = ");
		range = input.nextInt();
		
		System.out.print(n1 + " " + n2 + " " + n3);
		for (i=3; i < range; i++) {
			
			sum = n1 + n2 + n3;
			System.out.print(" " + sum); // 1 1 1
			
			n1 = n2;// n1 = 1
			n2 = n3;
			n3 = sum; // n2 = 1
		}
		
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve3() {
		
		Scanner input = new Scanner(System.in);
		int i, j, range, bilPrima;
		
		System.out.print("Input = ");
		range = input.nextInt();
		
		for (i=0; i < range; i++){
            
			bilPrima=0;
            
			for (j=1; j<=i; j++){
                if (i%j==0){
                    bilPrima=bilPrima+1;
                }
            }
            
            if (bilPrima==2){
                System.out.print(i+" ");
            }             
        }
		
		
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve4() {
		
		int input, i = 2, hasil = 0;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Input angka : ");
		input = sc.nextInt();
		
		if(input == 1) {
			System.out.print(input + "/" + (i-1) + " = " + input);
		}
		
		
		while (i <= input) {
			
//			if (input == 1) {
//				break;
//			}
			
			if (input % i == 0) {
				hasil = input/i;
				System.out.print(input + "/" + i + " = " + hasil);
				System.out.println();
				
				input = hasil;
				i = 2;
				continue; // melewati 1 statment di bawahnya.... Jika IF = FALSE
			}
			i++; // gak dijalanin kalau continue = true ... Maka i++ = TRUE
			
		}

	}
	
	//////////////////////////////////////////////////
	
	public static void resolve5() {
		
		
		int i, bensin, totalJarak = 0, batasBensin = 2500;
		String plus = "";
		String jarakTempuh = "";
		String[] jumlahJarak = new String[4];
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Input Jarak Tempuh = ");
		jarakTempuh = input.nextLine();
		
		String[] jarak = jarakTempuh.split(" ");
		
//		System.out.println(Arrays.toString(jarak));
		
		System.out.print("Jarak Tempuh = ");
		
		for (i=0; i < jarak.length; i++) {
			
//			if(i < jarak.length) {
//				plus += "+";
//			}
//			else {
//				plus = " ";
//			}
			
			switch(jarak[i]) {

				case "1" :	totalJarak += 2000;
							jumlahJarak[i] = "2 km";
							break;
				
				case "2" :	totalJarak += 500;
							jumlahJarak[i] = "500 m";
							break;
				
				case "3" :	totalJarak += 1500;
							jumlahJarak[i] = "1.5 km";
							break;
				
				case "4" : 	totalJarak += 300;
							jumlahJarak[i] = "300 m";
							break;
			
			}
			
//			String jarakFinal = jumlahJarak[i] + " + ";
//			jarakFinal.substring(0, jarakFinal.length() - 1);
			
			System.out.print(jumlahJarak[i] + plus);
			
			if (i != (jarak.length - 1)) {
				System.out.print(" + ");
			}
			else {
				
				System.out.print(" = " + (double)totalJarak/1000 + "km");
				
			}
		
			
			
		}
		
		while (totalJarak > batasBensin) {
			batasBensin += 2500;
		}
		bensin = 	batasBensin/2500;
		
		System.out.println();
		System.out.println("Bensin = " + bensin + " Liter");
	
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve6() {
		
		Scanner input = new Scanner(System.in);
		
		String hargaBaju = "";
		String hargaCelana = "";
		int uang, i, j,x;
		int hargaBeli;
		
		System.out.print("Uang Andi = ");
		uang = input.nextInt();
		
		input.nextLine();
		
		System.out.print("Harga Baju = ");
		hargaBaju = input.nextLine();
		
		System.out.print("Harga Celana = ");
		hargaCelana = input.nextLine();
		
		// Cara instant merubah String Array menjadi Int Array
		int[] hargaBajuInt = Arrays.stream(hargaBaju.split(",")).mapToInt(Integer::parseInt).toArray();
		int[] hargaCelanaInt = Arrays.stream(hargaCelana.split(",")).mapToInt(Integer::parseInt).toArray();
		
//		System.out.println(Arrays.toString(hargaBajuInt));
//		System.out.println(Arrays.toString(hargaCelanaInt));
		
		int[] totalHarga = new int[hargaBajuInt.length];
		int[] selisihHarga = new int[hargaBajuInt.length];
				
		for (i = 0; i < hargaBajuInt.length; i++) {
			
			totalHarga[i] = hargaBajuInt[i] + hargaCelanaInt[i];
			selisihHarga[i] =  uang - totalHarga[i];
//			System.out.print(totalHarga[i] + " ");
//			System.out.print(selisihHarga[i] + " ");					
		}
		
		hargaBeli = selisihHarga[0]; //8
		int total = totalHarga[0];
		
		for (j = 1; j < hargaBajuInt.length; j++) {
		
			if (hargaBeli >= 0) {
				
				if(hargaBeli > selisihHarga[j] && selisihHarga[j] >= 0) {
					
					hargaBeli = selisihHarga[j];
					total = totalHarga[j];
				}
				else {
					hargaBeli = hargaBeli;
					total = total;
				}
				
				
			}		
			
			else {
		
				hargaBeli = selisihHarga[j];
				total = totalHarga[j];
			}
		
		}

		
		System.out.println();
		System.out.print("Harga Baju yang cocok = " + total);		
		

	}
	
	//////////////////////////////////////////////////
	
	public static void resolve7() {
		
		Scanner input = new Scanner(System.in);
		
		String arr = "";
		int i,j, rotasi;
		
		System.out.print("Input Array = ");
		arr = input.nextLine();
		String[] arrSplit = arr.split(",");
		
		
		System.out.print("Rotasi : ");
		rotasi = input.nextInt();
		
		
		System.out.println(Arrays.toString(arrSplit));
		
		for (i=1; i <= rotasi; i++) {
			
			System.out.print(i + ": ");
			
			for (j=0; j < arrSplit.length; j++) {
				if(j+i >= arrSplit.length) {
					System.out.print(arrSplit[Math.abs(arrSplit.length - (i+j))] + " ");
				}
				else {
					System.out.print(arrSplit[j+i] + " ");
				}
			
			
			}
			System.out.println();
		
		}
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve8() {
		
		Scanner input = new Scanner(System.in);
		int puntung, batang, sisa, jualRokok;
		
		System.out.print("Banyak puntung rokok : ");
		puntung = input.nextInt();
		
		batang = puntung / 8;
		sisa = batang % 8;
		
		System.out.print("Batang rokok yang dirangkai : " + batang);
		System.out.println();
		System.out.print("Sisa puntung rokok : " + sisa);
		System.out.println();
		jualRokok = batang*500;
		
		System.out.print("Harga rokok per batang : " + jualRokok);
		
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve9() {
		
		Scanner input = new Scanner(System.in);
		
		int totalMenu = 0, indexAlergi, uangElsa = 0, totalHarga = 0;
		int hargaMenu = 0, totalHargaElsa = 0;
		int i,j;
		String hargaMenuString = "";
		
		
		System.out.print("Total Menu : ");
		totalMenu = input.nextInt();
		
		System.out.print("Makanan alergi index ke : ");
		indexAlergi = input.nextInt();
		
		input.nextLine();
		
		System.out.print("Harga Menu : ");
		hargaMenuString = input.nextLine();
		
		String[] hargaMenuSplit = hargaMenuString.split(",");
		int simpanHarga = 0;
		
//		System.out.print(Arrays.toString(hargaMenu));
		
		System.out.print("Uang Elsa : ");
		uangElsa = input.nextInt();
		
		
		System.out.print("Total makanan yang dimakan Dimas dan elsa : ");
		for (i=0; i < totalMenu; i++) {
			
			System.out.print(hargaMenuSplit[i] + " ");
			simpanHarga = Integer.parseInt(hargaMenuSplit[i]);
//			hargaMenu = Integer.parseInt(hargaMenuString);
			totalHarga += simpanHarga;
			
			if (i != (hargaMenuSplit.length - 1)) {
				System.out.print(" + ");
			}
			else {
				
				System.out.print(" = " + totalHarga);
				
			}
			
			if (simpanHarga == Integer.parseInt(hargaMenuSplit[indexAlergi])) {
				simpanHarga = 0;
			}
			
			totalHargaElsa += simpanHarga;
			
		}
		System.out.println();
		System.out.print("Total Harga untuk Elsa = " + totalHargaElsa);
		System.out.println();
		totalHargaElsa = totalHargaElsa / 2;
		
		System.out.print("Harga makanan Elsa = " + totalHargaElsa);
		
		int sisa = 0;
		sisa = uangElsa - totalHargaElsa;
		System.out.println();
		System.out.println("Sisa uang Elsa = " + sisa);
	
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve10() {
		
		Scanner input = new Scanner(System.in);
		String kalimat = "";
		int i;
		
		
		System.out.print("Input : ");
		kalimat = input.nextLine();
		
		char[] charKalimat = kalimat.toCharArray();
		
		Arrays.sort(charKalimat);
		
		String vocal = "";
		String konsonan = "";
		
		for (i=0; i < charKalimat.length; i++) {
			if(charKalimat[i] == 'a' || charKalimat[i] == 'i' || charKalimat[i] == 'u' || charKalimat[i] == 'e' || charKalimat[i] == 'o'|| charKalimat[i] == 'A'|| charKalimat[i] == 'I'|| charKalimat[i] == 'U'|| charKalimat[i] == 'E' || charKalimat[i] == 'O') {
				
				vocal += String.valueOf(charKalimat[i]);	
			
			}
			else if (charKalimat[i] == ' '){
				
				vocal = vocal;
			
			}
			else {
				konsonan += String.valueOf(charKalimat[i]);
			}
		}
		
		System.out.println("Huruf Vokal = " + vocal);
		System.out.print("Huruf Konsonan = " + konsonan);
	}
	
	//////////////////////////////////////////////////	
}














