package TugasDay7;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Case07 {

	public static void resolve1() {
		
		Scanner input = new Scanner(System.in);
		
		int i;
		
		String tanggalParkir = "";
		String tanggalPulang = "";
		String jamParkir = "";
		String jamPulang = "";
		
		System.out.print("Input tanggal parkir (dd/mm/yyyy) = ");
		tanggalParkir = input.nextLine();
		
		System.out.print("Input jam parkir (hh.mm) = ");
		jamParkir = input.nextLine();
//		System.out.println(jamParkir);

		System.out.print("Input jam pulang (hh.mm) = ");
		jamPulang = input.nextLine();
//		System.out.println(jamPulang);
		
		System.out.print("Input tanggal parkir (dd/mm/yyyy) = ");
		tanggalPulang = input.nextLine();
		
		int [] tanggalParkirInt = Arrays.stream(tanggalParkir.split("/")).mapToInt(Integer::parseInt).toArray();
		int [] tanggalPulangInt = Arrays.stream(tanggalPulang.split("/")).mapToInt(Integer::parseInt).toArray();
		int [] jamParkirInt = Arrays.stream(jamParkir.split("\\.")).mapToInt(Integer::parseInt).toArray();
		int [] jamPulangInt = Arrays.stream(jamPulang.split("\\.")).mapToInt(Integer::parseInt).toArray();
		
//		System.out.println(Arrays.toString(tanggalParkirInt));
//		System.out.println(Arrays.toString(tanggalPulangInt));
//		System.out.println(Arrays.toString(jamParkirInt));
//		System.out.print(Arrays.toString(jamPulangInt));
		
		Calendar kalenderParkir = Calendar.getInstance();
		kalenderParkir.set(tanggalParkirInt[2], tanggalParkirInt[1]-1, tanggalParkirInt[0], jamParkirInt[0], jamParkirInt[1], 0);
		
		Calendar kalenderPulang = Calendar.getInstance();
		kalenderPulang.set(tanggalPulangInt[2], tanggalPulangInt[1]-1, tanggalPulangInt[0], jamPulangInt[0], jamPulangInt[1], 0);
	
		
//		System.out.println(kalenderParkir);
//		System.out.print(kalenderPulang);
		long tanggalParkirLong = kalenderParkir.getTimeInMillis();
		long tanggalPulangLong = kalenderPulang.getTimeInMillis();
		
		long miliDetik = tanggalPulangLong-tanggalParkirLong;

		long menit1 = TimeUnit.MILLISECONDS.toMinutes(miliDetik);

		long jam = menit1 / 60;
		long menit = menit1 % 60;
		
		if (menit > 0) {
			jam++;
		}
		

		long batasBayar = 1;
		long bayar = 0;
		
		if (jam > batasBayar) {
			bayar = jam*3000;
		}
		else if (jam > 0 && jam <=1){
			bayar = 3000;
		}
		
		System.out.print("Bayar Parkir = Rp" + bayar);
		
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve2() {
		
		Scanner input = new Scanner(System.in);
		
		int i;
		
		String tanggalPinjam = "";
		String tanggalKembali = "";
		
		long tanggalPinjamLong;
		long tanggalKembaliLong;
		long hariDenda = 0, denda = 0;
		long batasDenda = 0; // 3 hari
		
		System.out.print("Input tanggal pinjam (dd-mm-yyyy) = ");
		tanggalPinjam = input.nextLine();
		
		System.out.print("Berapa hari pinjam (hari) = ");
		batasDenda = input.nextLong();
		
		input.nextLine();
		
		System.out.print("Input tanggal kembalikan (dd-mm-yyyy) = ");
		tanggalKembali = input.nextLine();
		
		System.out.print("");
		int [] tanggalPinjamInt = Arrays.stream(tanggalPinjam.split("-")).mapToInt(Integer::parseInt).toArray();
		int [] tanggalKembaliInt = Arrays.stream(tanggalKembali.split("-")).mapToInt(Integer::parseInt).toArray();
		
//		System.out.print(Arrays.toString(tanggalPinjamInt));
//		System.out.print(Arrays.toString(tanggalKembaliInt));
		
		Calendar kalenderPinjam = Calendar.getInstance();
		kalenderPinjam.set(tanggalPinjamInt[2], (tanggalPinjamInt[1]-1), tanggalPinjamInt[0]); //01-01-1970
		
		Calendar kalenderKembali = Calendar.getInstance();
		kalenderKembali.set(tanggalKembaliInt[2], (tanggalKembaliInt[1]-1), tanggalKembaliInt[0]); //01-01-1970
		
		tanggalPinjamLong = kalenderPinjam.getTimeInMillis(); // waktu dari 01-01-1970 sampai tanggal pinjam
		tanggalKembaliLong = kalenderKembali.getTimeInMillis(); // waktu dari 01-01-1970 sampai tanggal kembali
		
		long miliDetik = tanggalKembaliLong-tanggalPinjamLong;
		
		long totalDay = TimeUnit.MILLISECONDS.toDays(miliDetik);
		
//		System.out.print(totalDay);
		
		
		if (totalDay > batasDenda) {
			hariDenda = totalDay - batasDenda;
			denda = hariDenda*500;
		}
		else {
			denda = 0;
		}
		
		System.out.println();
		System.out.println("Hari Denda = " + hariDenda);
		System.out.println("Lama pinjam buku = " + totalDay);
		System.out.print("Total Denda = " + denda);
		
	}
	
	//////////////////////////////////////////////////
	
	public static int resolve3Bulan(String bulan) {
		if (bulan.equalsIgnoreCase("Januari")) {
			return 0;
		}
		else if (bulan.equalsIgnoreCase("Februari")) {
			return 1;
		}
		else if (bulan.equalsIgnoreCase("Maret")) {
			return 2;
		}
		else if (bulan.equalsIgnoreCase("April")) {
			return 3;
		}
		else if (bulan.equalsIgnoreCase("Mei")) {
			return 4;
		}
		else if (bulan.equalsIgnoreCase("Juni")) {
			return 5;
		}
		else if (bulan.equalsIgnoreCase("Juli")) {
			return 6;
		}
		else if (bulan.equalsIgnoreCase("Agustus")) {
			return 7;
		}
		else if (bulan.equalsIgnoreCase("September")) {
			return 8;
		}
		else if (bulan.equalsIgnoreCase("Oktober")) {
			return 9;
		}
		else if (bulan.equalsIgnoreCase("November")) {
			return 10;
		}
		else if (bulan.equalsIgnoreCase("Desember")) {
			return 11;
		}
		
		return -1;
	}
	
	public static String resolve3BulanInt(int bulan) {
		if (bulan == 0) {
			return "Januari";
		}
		else if (bulan == 1) {
			return "Februari";
		}
		else if (bulan == 2) {
			return "Maret";
		}
		else if (bulan == 3) {
			return "April";
		}
		else if (bulan == 4) {
			return "Mei";
		}
		else if (bulan == 5) {
			return "Juni";
		}
		else if (bulan == 6) {
			return "Juli";
		}
		else if (bulan == 7) {
			return "Agustus";
		}
		else if (bulan == 8) {
			return "September";
		}
		else if (bulan == 9) {
			return "Oktober";
		}
		else if (bulan == 10) {
			return "November";
		}
		else if (bulan == 11) {
			return "Desember";
		}
		
		return null;
	}
	
	
	public static void resolve3() {
		
		Scanner input = new Scanner(System.in);
		String mulai = "";
		String libur = "";
		int bulan = -1;
		
		int i, lamaKelas = 0;
		int [] liburInt = {};
		Calendar tanggalMulai = Calendar.getInstance();
		Calendar tglSekarang = Calendar.getInstance();
		
		while (bulan == -1) {
		
			System.out.print("Tanggal Mulai (dd MONTH yyyy) : ");
			mulai = input.nextLine();
			
			System.out.print("Lama Kelas (hari) : ");
			lamaKelas = input.nextInt();
			
			input.nextLine();
			
			System.out.print("Tanggal Libur : ");
			libur = input.nextLine();
			
			
			String [] mulaiString = mulai.split(" "); //split mulaiString 14 Agustus 2020 [14, Agustus, 2020] = String
			
			
			liburInt = Arrays.stream(libur.split(",")).mapToInt(Integer::parseInt).toArray(); // split tanggal (String) 17,20 = [17,20]
		
//			System.out.print(Arrays.toString(mulaiString));
		
			bulan = resolve3Bulan(mulaiString[1]); // ngambil fungsi dari resolve3Bulan(); //
			// Agustus = bulan ke 7, soalnya bulan mulai dari 0
			tanggalMulai.set(Integer.parseInt(mulaiString[2]), bulan, Integer.parseInt(mulaiString[0])); //(2020, 7, 14)
//			System.out.print(tanggalMulai);
			
			tglSekarang.set(Integer.parseInt(mulaiString[2]), bulan, Integer.parseInt(mulaiString[0])); //(2020,7,14)
			
//			System.out.println(tglSekarang.get(Calendar.DAY_OF_WEEK));
		
		}
		
		int durasi = 0; // durasi = 9, lamaKelas = 10
		
		//17,20
		
		while(durasi < lamaKelas) { //14 1 < 15 //true karena durasi = 0 dan lamaKelas = 10
			
			boolean isExist = false; 
			
			for(i=0; i <liburInt.length; i++) { // i = 0; i < 2 hari libur, i++
				// DATE return nya tanggal = 14.. apakah 14 == 17? 
				if (tglSekarang.get(Calendar.DATE) == liburInt[i]) { //14, libur=17,20
					isExist = true; // ternyata BENAR tanggal tersebut hari LIBUR
				}
				
			}
			
			//tglSekarang.get(Calendar.Day_OF_WEEk) 
			if(tglSekarang.get(Calendar.DAY_OF_WEEK) != 7 && tglSekarang.get(Calendar.DAY_OF_WEEK) != 1 && !isExist) {
				// 14(6) benar									14(6) benar									true
				// 15(7) salah									15 benar									true
				// 16 benar										16 salah									true
				// 17(2) benar									17(2) benar									false
				// 18 benar										18 benar									true
				// 19 benar										19 benar									true
				// 20 benar										20 benar									false
				durasi++; // pokonya harus bukan hari libur baru durasi++ nya jalan.
			}
			
			if (durasi < lamaKelas) {
				tglSekarang.add(Calendar.DATE, 1); //14 >>> 15
			}
			
//			System.out.println(tglSekarang.get(Calendar.DATE));
			
		}
		
		System.out.print("Kelas selesai pada : " + 
							tglSekarang.get(Calendar.DATE) + " " +
							resolve3BulanInt(tglSekarang.get(Calendar.MONTH)) + " " +
							tglSekarang.get(Calendar.YEAR));
	
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve4() {
		
		Scanner input = new Scanner(System.in);
		
		String teks = "";
		String palindrom = "";
		int i;
		
		System.out.print("Input teks = ");
		teks = input.nextLine();
	
		for (i=teks.length()-1; i>-1; i--) {
			char teksChar = teks.charAt(i);
			palindrom += String.valueOf(teksChar);
		}
		
		if (palindrom.equalsIgnoreCase(teks)) {
			System.out.print(" ` " + palindrom + " ` " + " adalah palindrom");
		}
		else {
			System.out.println(" ` " + teks + " ` " + " bukan palindrom");
		}
		
		
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve5() {
		
		
		int i, bensin, totalJarak = 0, batasBensin = 2500;
		String plus = "";
		String jarakTempuh = "";
		String[] jumlahJarak = new String[4];
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Input Jarak Tempuh = ");
		jarakTempuh = input.nextLine();
		
		String[] jarak = jarakTempuh.split(" ");
		
//		System.out.println(Arrays.toString(jarak));
		
		System.out.print("Jarak Tempuh = ");
		
		for (i=0; i < jarak.length; i++) {
			
			
			switch(jarak[i]) {

				case "1" :	totalJarak += 2000;
							jumlahJarak[i] = "2 km";
							break;
				
				case "2" :	totalJarak += 500;
							jumlahJarak[i] = "500 m";
							break;
				
				case "3" :	totalJarak += 1500;
							jumlahJarak[i] = "1.5 km";
							break;
				
				case "4" : 	totalJarak += 300;
							jumlahJarak[i] = "300 m";
							break;
			
			}

			
			System.out.print(jumlahJarak[i] + plus);
			
			if (i != (jarak.length - 1)) {
				System.out.print(" + ");
			}
			else {
				
				System.out.print(" = " + (double)totalJarak/1000 + "km");
				
			}
		
			
			
		}
		
		while (totalJarak > batasBensin) {
			batasBensin += 2500;
		}
		bensin = 	batasBensin/2500;
		
		System.out.println();
		System.out.println("Bensin = " + bensin + " Liter");
	
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve6() {
		
		Scanner input = new Scanner(System.in);
		int cek = 0, i;
		
		String sos = "";
		
		System.out.print("Input Code = ");
		sos = input.nextLine().toUpperCase();
//		char [] pecahSOS = new char[sos.length()];
		
		
		// Misal input SOSSOSSOS = salah 0
		// Misal input SOSPSPAAS = salah 2
		// Misal input ABCPQSSQP = salah 3
		
		// ABC PQS cek = 2, SQP cek = 2, 3
		for (i=0; i<sos.length(); i+=3) {
		
			if (sos.charAt(i) != 'S'){ 
				cek++; 
				continue;
			}
			
			if (sos.charAt(i+1) != 'O') {
				cek++;
				if (sos.charAt(i) != 'S'){ 
					cek--; 
					continue;
				}
				
			}
			
			if (sos.charAt(i+2) != 'S') {  
				cek++;
				if (sos.charAt(i+1) != 'O'){ 
					cek --;
					continue;
				}
				
			}
			
			
		}
		
		System.out.print("Kode yang salah ada = " + cek);
		

	}	
		
	//////////////////////////////////////////////////
	
}


	
	
	
	
	