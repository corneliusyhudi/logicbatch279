package TugasDay7;

import java.util.Scanner;

public class Main {

public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		String answer = "Y";
		
		while(answer.toUpperCase().equals("Y")) {
			try {
				
				System.out.print("Pilih nomor tugas 1-10 : ");
				int number = input.nextInt();
				input.nextLine();
				
				switch (number) {
				case 1:
					Case07.resolve1();
					break;
				case 2 :
					Case07.resolve2();
					break;
				case 3 :
					Case07.resolve3();
					break;
				case 4 :
					Case07.resolve4();
					break;
				case 5 :
					Case07.resolve5();
					break;
				case 6 :
					Case07.resolve6();
					break;
//				case 7 :
//					Case07.resolve7();
//					break;
//				case 8 :
//					Case07.resolve8();
//					break;
//				case 9 :
//					Case07.resolve9();
//					break;
//				case 10 :
//					Case07.resolve10();
//					break;
//				default:
//					System.out.println("Nomor Tugas tidak ada!");
//					break;
				}
				
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
			System.out.println();
			System.out.println("Continue? (Y/N)");
			answer = input.nextLine();
		}
			
	}


}
