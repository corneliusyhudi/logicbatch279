package TugasDay9;

import java.util.Scanner;


public class Main {

	public static void main(String[] args) {
			
			Scanner input = new Scanner(System.in);
			String answer = "Y";
			
			while(answer.toUpperCase().equals("Y")) {
				try {
					
					System.out.print("Pilih nomor tugas 1-10 : ");
					int number = input.nextInt();
					input.nextLine();
					
					switch (number) {
					case 1:
						Case09.resolve1();
						break;
					case 2 :
						Case09.resolve2();
						break;
					case 3 :
						Case09.resolve3();
						break;
					case 4 :
						Case09.resolve4();
						break;
					case 5 :
						Case09.resolve5();
						break;
//					case 6 :
//						Case09.resolve6();
//						break;
//					case 7 :
//						Case09.resolve7();
//						break;
//					case 8 :
//						Case09.resolve8();
//						break;
//					case 9 :
//						Case09.resolve9();
//						break;
//					case 10 :
//						Case09.resolve10();
//						break;
//					default:
//						System.out.println("Nomor Tugas tidak ada!");
//						break;
					}
					
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				
				System.out.println();
				System.out.println("Continue? (Y/N)");
				answer = input.nextLine();
			}
				
		}
}
