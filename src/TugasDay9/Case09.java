package TugasDay9;

import java.util.*;

public class Case09 {
	
	public static void resolve1() {
		
		Scanner input = new Scanner(System.in); 
		int point = 0, taruhan = 0, i;
		char yes = 'y';
		char tebak;
		
		Random rand = new Random(); 
		 
		// Will work for [0 - 9]. 
		int randNumber = rand.nextInt(10); 
		
		
		System.out.print("Input Point = ");
		point = input.nextInt();
		
		do {
			taruhan = 0;
			System.out.print("Taruhan = ");
			taruhan = input.nextInt();
			
			if(taruhan <= point) {
			
				
				System.out.print("Tebak (U/D) = ");
				tebak = input.next().toUpperCase().charAt(0);
				
				System.out.println(randNumber);
				
				if (tebak == 'U' && randNumber > 5) {
					point = point+taruhan;
					System.out.println("You WIN!");
					System.out.print("Point saat ini = " + point);
				} else if (tebak == 'U' && randNumber < 5) {
					point = point-taruhan;
					System.out.println("You LOSE!");
					System.out.print("Point saat ini = " + point);
				}  else if (tebak == 'D' && randNumber < 5) {
					point = point+taruhan;
					System.out.println("You WIN!");
					System.out.print("Point saat ini = " + point);
				}  else if (tebak == 'D' && randNumber > 5) {
					point = point-taruhan;
					System.out.println("You LOSE!");
					System.out.print("Point saat ini = " + point);
				}  else if ((tebak == 'U' && randNumber == 5) || (tebak == 'D' && randNumber == 5)) {
					point = point;
					System.out.println("DRAW!");
					System.out.print("Point saat ini = " + point);
				} 
			
			} else {
				System.out.print("Taruhan tidak sesuai dengan point!");
			}
			
			
			System.out.println();
			System.out.println("Try Again? (Y/N) = ");
			yes = input.next().toLowerCase().charAt(0);
			
			
			
		} while(point > 0 && yes == 'y');
		
		if(point <= 0) {
			System.out.print("Tidak bisa melanjutkan permainan. Point anda habis!");
		}
	}
		
	//////////////////////////////////////////////////
	
	public static void resolve2() {
	
		Scanner input = new Scanner(System.in);
	
		int i, total = 0, bawa = 0;
		String[] keranjang = new String[4];		
		
		for (i=1; i<=3; i++) {
			System.out.print("Keranjang " + i + " = ");
			keranjang[i] = input.nextLine();
			
		}
		
		System.out.print("Keranjang yang akan dibawa : ");
		
		bawa = input.nextInt();
		
		switch(bawa) {
			
			case 1 : 
				
				for(i=1; i<=3; i++) {
					if(keranjang[i].equalsIgnoreCase("kosong")) {
						keranjang[i] = "0";
					}
				}
				
				keranjang[1] = "0";
				
				for(i=1; i<=3; i++) {
					total += Integer.parseInt(keranjang[i]);
				}
				System.out.println("Sisa buah = " + total);
				break;
				
			case 2 : 
				
				for(i=1; i<=3; i++) {
					if(keranjang[i].equalsIgnoreCase("kosong")) {
						keranjang[i] = "0";
					}
				}
				
				keranjang[2] = "0";
				
				for(i=1; i<=3; i++) {
					total += Integer.parseInt(keranjang[i]);
				}
				System.out.println("Sisa buah = " + total);
				break;
				
			case 3 : 
				
				for(i=1; i<=3; i++) {
					if(keranjang[i].equalsIgnoreCase("kosong")) {
						keranjang[i] = "0";
					}
				}
				
				keranjang[3] = "0";
				
				for(i=1; i<=3; i++) {
					total += Integer.parseInt(keranjang[i]);
				}
				System.out.println("Sisa buah = " + total);
				break;
				
				
			default :
				
				for(i=1; i<=3; i++) {
					if(keranjang[i].equalsIgnoreCase("kosong")) {
						keranjang[i] = "0";
					}
				}
				
				
				for(i=1; i<=3; i++) {
					total += Integer.parseInt(keranjang[i]);
				}
				System.out.println("Sisa buah = " + total);
				break;
		
		}
		
		
		
	}

	//////////////////////////////////////////////////
	
	public static void resolve3() {
		
		Scanner input = new Scanner(System.in);
		
		int i, j, n, tempTotal, total, totalOutput = 0;
		String angka = "";
		char[] angkaChar = {};
		char[] angkaCharIndex = {};
		int[] angkaInt = new int[1000];
		
		System.out.print("Input = ");
		n = input.nextInt();
		
		for (i=100; i<=1000; i++) { 
			
			tempTotal = i; // tempTotal = 100
			
			while(true) { // looping forever (berhenti ketika da kondisi if d bawah) if (total==1) atau if (total%10 == total) > break;
				angka = String.valueOf(tempTotal); // String = 100
				angkaChar = angka.toCharArray(); // String 100 > char 100
				total = 0; // mulai dari 0
				for(j=0; j<angka.length(); j++) {
					angkaChar[j] = angka.charAt(j); // ngambil per index dari angkaChar.
					
					total += Math.pow(Character.getNumericValue(angkaChar[j]), 2); // di convert per index dari angkaChar ke integer lalu d pow (pangkat)
					// 109 <=> 1^2 + 0^2 + 9^2 = 82
					// 82 <=>
				}
				
				if(total == 1) {
					System.out.println(i + " is The One Number" );
					totalOutput++; // faktor berhentinya forloop yang 1000
					break;
				}
				else if(total%10 == total) {
//					System.out.println(i + " is not The One Number" );
					break;
				}
				
				else {
					tempTotal = total; // 82 masuk ke sini. tempTotal = 109, diubah jadi tempTotal = 82
				}
				
			}
			
			if(totalOutput == n) {
				break;
			}
			
		}
		
		
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve4() {
		
		int number = 0;
        long factorial = 1;
        Scanner input = new Scanner(System.in);
        
        System.out.print("Input angka = ");
        number = input.nextInt();
        
        System.out.print(number + "!" + " = ");
        for(int i = number; i >= 1; i--)
        {
            // factorial = factorial * i;
            
        	factorial *= i;
        	
        	System.out.print(i);
        	
        	if(i <= 1) {
        		System.out.print(" = " + factorial);
        	}
        	else {
        		System.out.print(" * ");
        	}
        	
        }
		
		
	}
	
	//////////////////////////////////////////////////
	
	public static void resolve5() {
		

        Scanner input = new Scanner(System.in);
        
        int priaDewasa = 0, wanitaDewasa = 0, anak = 0, bayi = 0;
        
        System.out.print("Pria Dewasa = ");
        priaDewasa = input.nextInt();
        
        System.out.print("Wanita Dewasa = ");
        wanitaDewasa = input.nextInt();
        
        System.out.print("Anak-anak = ");
        anak = input.nextInt();
        
        System.out.print("Bayi = ");
        bayi = input.nextInt();
        
		int bajuPriaDewasa = priaDewasa*1;
		int bajuWanitaDewasa = wanitaDewasa*2;
		int bajuAnak = anak*3;
		int bajuBayi = bayi*5;
		
		int sumBaju = 0;
		sumBaju = bajuPriaDewasa + bajuWanitaDewasa + bajuAnak + bajuBayi;
		
		if(sumBaju > 10 && sumBaju % 2 != 0) {
			sumBaju = sumBaju + wanitaDewasa;
			System.out.println(sumBaju + " Baju");
		}
		else {
			System.out.println(sumBaju + " Baju");
		}
		
	}
	
}