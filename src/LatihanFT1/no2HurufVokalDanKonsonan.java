package LatihanFT1;

import java.util.Arrays;
import java.util.Scanner;

public class no2HurufVokalDanKonsonan {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String kalimat = "";
		int i;
		
		
		System.out.print("Input : ");
		kalimat = input.nextLine();
		
		char[] charKalimat = kalimat.toCharArray();
		
		Arrays.sort(charKalimat);
		
		String vocal = "";
		String konsonan = "";
		
		for (i=0; i < charKalimat.length; i++) {
			if(charKalimat[i] == 'a' || charKalimat[i] == 'i' || charKalimat[i] == 'u' || charKalimat[i] == 'e' || charKalimat[i] == 'o'|| charKalimat[i] == 'A'|| charKalimat[i] == 'I'|| charKalimat[i] == 'U'|| charKalimat[i] == 'E' || charKalimat[i] == 'O') {
				
				vocal += String.valueOf(charKalimat[i]);	
			
			}
			else if (charKalimat[i] == ' '){
				
				vocal = vocal;
			
			}
			else {
				konsonan += String.valueOf(charKalimat[i]);
			}
		}
		
		System.out.println("Huruf Vokal = " + vocal);
		System.out.print("Huruf Konsonan = " + konsonan);
	}
	
}
