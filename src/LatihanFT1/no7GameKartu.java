package LatihanFT1;

import java.util.Random;
import java.util.Scanner;

public class no7GameKartu {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); 
		int kartu = 0, taruhan = 0, i;
		char yes = 'y';
		char tebak;
		
		Random rand = new Random(); 
		 
		// Will work for [0 - 9]. 
//		int randNumber = rand.nextInt(10); 
		
		
		System.out.print("Jumlah Kartu = ");
		kartu = input.nextInt();
		
		do {
			taruhan = 0;
			System.out.print("Taruhan = ");
			taruhan = input.nextInt();
			
			if(taruhan <= kartu) {
			
				System.out.print("Tebak Kotak (A/B) = ");
				tebak = input.next().toUpperCase().charAt(0);
				
				int kotakA = rand.nextInt(10);
				int kotakB = rand.nextInt(10);
				
				System.out.print("Kotak A = " + kotakA + "\n");
				System.out.print("Kotak B = " + kotakB + "\n");
				
				if (tebak == 'A') {
					if(kotakA > kotakB) {
						kartu = kartu+taruhan;
						System.out.println("You WIN!");
						System.out.print("kartu saat ini = " + kartu);
					}
					else if (kotakA < kotakB){
						kartu = kartu-taruhan;
						System.out.println("You LOSE!");
						System.out.print("kartu saat ini = " + kartu);
					}
					else if(kotakA == kotakB) {
						kartu = kartu;
						System.out.println("DRAW!");
						System.out.print("kartu saat ini = " + kartu);
					}
					
				} 
				
				else if (tebak == 'B') {
					if(kotakA > kotakB) {
						kartu = kartu-taruhan;
						System.out.println("You LOSE!");
						System.out.print("kartu saat ini = " + kartu);
					}
					else if (kotakA < kotakB){
						kartu = kartu+taruhan;
						System.out.println("You WIN!");
						System.out.print("kartu saat ini = " + kartu);
					}
					else if(kotakA == kotakB) {
						kartu = kartu;
						System.out.println("DRAW!");
						System.out.print("kartu saat ini = " + kartu);
					}
			
				} 
			}	
			
			else {
				System.out.print("Taruhan tidak sesuai dengan banyak kartu!");
			}
			
			
			System.out.println();
			System.out.println("Try Again? (Y/N) = ");
			yes = input.next().toLowerCase().charAt(0);
			
			if (yes == 'n') {
				System.out.println("GAME OVER !");
			}
			
		} while(kartu > 0 && yes == 'y');
		
		if(kartu <= 0) {
			System.out.print("Tidak bisa melanjutkan permainan. Kartu anda habis!");
		}
	}
}
