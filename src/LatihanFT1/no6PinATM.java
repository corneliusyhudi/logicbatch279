package LatihanFT1;

import java.util.*;

public class no6PinATM {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int rek = 0;
		
		System.out.print("Masukka PIN : ");
		int pin = input.nextInt();
		
		System.out.print("Input Uang yang disetor : ");
		int setor = input.nextInt();
		
		System.out.println("Pilih Transfer : ");
		System.out.println("1. Antar Rekening\t 2. Antar Bank");
		
		System.out.print("Input pilihan : ");
		int pilihan = input.nextInt();
		
		if (pin == 123456 && pilihan == 1) {
			System.out.print("Masukkan Rekening Tujuan : ");
			rek = input.nextInt();
			System.out.print("Masukkan nominal transfer : ");
			int tf = input.nextInt();
			
			if(setor >= tf) {
				setor = setor - tf;
				System.out.println("Transaksi berhasil, saldo anda saat ini Rp." + setor);
			}
			else {
				System.out.println("Saldo anda tidak mencukupi");
			}
		}
		
		else if (pin == 123456 && pilihan == 2) {
			System.out.print("Masukkan Kode Bank : ");
			int cb = input.nextInt();
			System.out.print("Masukkan Rekening Tujuan : ");
			rek = input.nextInt();
			System.out.print("Masukkan nominal transfer : ");
			int tf = input.nextInt();
			
			if(setor >= tf+7500) {
				setor = setor - tf;
				System.out.println("Transaksi berhasil, saldo anda saat ini Rp." + (setor-7500));
			}
			else {
				System.out.println("Saldo anda tidak mencukupi");
			}
		}
		
		else {
			System.out.println("PIN SALAH !");
		}
		
		
	}
	
}
