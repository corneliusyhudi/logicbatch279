package LatihanFT1;

import java.util.*;

public class no9GunungLembah {
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		int gunung = 0;
		int lembah = 0;
		int i;
		String perjalanan;
		
		System.out.print("Input perjalanan Ninja Hatorri : ");
				
		perjalanan = input.nextLine();

		String arr[] = perjalanan.split(" ");
		
		for(i=0; i < arr.length-1; i++) {
			if(arr[i].equalsIgnoreCase("N") && arr[i+1].equalsIgnoreCase("T")) {
				gunung++;
			}
			else if(arr[i].equalsIgnoreCase("T") && arr[i+1].equalsIgnoreCase("N")) {
				lembah++;
			}
			
		}
		
		System.out.print("Gunung = " + gunung + "\n");
		System.out.print("Lembah = " + lembah + "\n");
	}

}
