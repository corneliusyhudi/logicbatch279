package LatihanFT1;

import java.util.Scanner;

public class no4GoFoodLinear {
	public static void main(String[] args) {
		int i, bensin, totalJarak = 0, batasBensin = 2500;
		String plus = "";
		String jarakTempuh = "";
		String[] jumlahJarak = new String[4];
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Input Jarak Tempuh = ");
		jarakTempuh = input.nextLine();
		
		String[] jarak = jarakTempuh.split(" ");
		
//		System.out.println(Arrays.toString(jarak));
		
		System.out.print("Jarak Tempuh = ");
		
		for (i=0; i < jarak.length; i++) {
			
			
			switch(jarak[i]) {

				case "1" :	totalJarak += 2000;
							jumlahJarak[i] = "2 km";
							break;
				
				case "2" :	totalJarak += 500;
							jumlahJarak[i] = "500 m";
							break;
				
				case "3" :	totalJarak += 1500;
							jumlahJarak[i] = "1.5 km";
							break;
				
				case "4" : 	totalJarak += 2500;
							jumlahJarak[i] = "2.5 km";
							break;
			
			}

			
			System.out.print(jumlahJarak[i] + plus);
			
			if (i != (jarak.length - 1)) {
				System.out.print(" + ");
			}
			else {
				
				System.out.print(" = " + (double)totalJarak/1000 + "km");
				
			}
		
			
			
		}
		
		while (totalJarak > batasBensin) {
			batasBensin += 2500;
		}
		bensin = 	batasBensin/2500;
		
		System.out.println();
		System.out.println("Bensin = " + bensin + " Liter");
	}
}
